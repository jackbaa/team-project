DROP DATABASE LTUBikes;
CREATE DATABASE LTUBikes;
USE LTUBikes;
DROP TABLE IF EXISTS User, ContactDetail, Trip, Preferences, Bike, Transaction, PaymentDetail, Accident;

CREATE TABLE User (
	ID INT AUTO_INCREMENT,
    FullName VARCHAR(25) NOT NULL,
	Username VARCHAR(15) UNIQUE NOT NULL,
    Email VARCHAR(45) UNIQUE NOT NULL,
    Password VARCHAR(25) NOT NULL,
    IsAdmin BOOLEAN DEFAULT 0,
    NightMode BOOLEAN DEFAULT 0,
    LoggedIn BOOLEAN DEFAULT 0,
    PRIMARY KEY (ID)
) ENGINE=InnoDB;

CREATE TABLE ContactDetail (
	UserID INT AUTO_INCREMENT,
    PostCode VARCHAR(10) UNIQUE NOT NULL,
	PhoneNumber VARCHAR(20) UNIQUE NOT NULL,
    Address VARCHAR(100) UNIQUE NOT NULL,
    PRIMARY KEY (UserID),
    FOREIGN KEY (UserID) REFERENCES User (ID)
) ENGINE=InnoDB;

CREATE TABLE Bike (
	ID INT AUTO_INCREMENT,
    Model VARCHAR(10) NOT NULL,
    Maintenance BOOLEAN DEFAULT 0,
    LastMaintenance TIMESTAMP,
    InUse BOOLEAN DEFAULT 0,
    UserID INT NULL,
    PRIMARY KEY (ID),
    FOREIGN KEY (UserID) REFERENCES User (ID)
) ENGINE=InnoDB;

CREATE TABLE Trip (
    ID INT AUTO_INCREMENT,
    TotalDist DECIMAL(4,2) UNSIGNED,
    AvgSpeed DECIMAL(4,2) UNSIGNED,
    CaloriesBurned DECIMAL(4,2) UNSIGNED,
    TotalHours DECIMAL(4,2) UNSIGNED,
    TotalSpend DECIMAL(4,2) UNSIGNED,
    UserID INT NOT NULL,
    BikeID INT NOT NULL,
    PRIMARY KEY (ID),
    FOREIGN KEY (UserID) REFERENCES User (ID),
    FOREIGN KEY (BikeID) REFERENCES Bike (ID)
) ENGINE=InnoDB;

CREATE TABLE Transaction (
	ID INT AUTO_INCREMENT,
    Payment DECIMAL(4,2) UNSIGNED,
    Balance DECIMAL(5,2) UNSIGNED,
    Date TIMESTAMP,
    UserID INT NOT NULL,
    TripID INT NOT NULL,
    PRIMARY KEY (ID),
    FOREIGN KEY (UserID) REFERENCES User (ID),
    FOREIGN KEY (TripID) REFERENCES Trip (ID)
) ENGINE=InnoDB;

CREATE TABLE PaymentDetail (
	UserID INT AUTO_INCREMENT,
    CardNumber VARCHAR(19) UNIQUE NOT NULL,
    ExpiryDate VARCHAR(5) UNIQUE NOT NULL,
    PRIMARY KEY (UserID),
    FOREIGN KEY (UserID) REFERENCES User (ID)
) ENGINE=InnoDB;

CREATE TABLE Accident (
    ID INT AUTO_INCREMENT,
    Description VARCHAR(100) NOT NULL,
    Injury VARCHAR(100) NOT NULL,
    Damage VARCHAR(100) NOT NULL,
    Date TIMESTAMP,
	UserID INT NOT NULL,
    BikeID INT NOT NULL,
    PRIMARY KEY (ID),
    FOREIGN KEY (UserID) REFERENCES User (ID),
    FOREIGN KEY (BikeID) REFERENCES Bike (ID)
) ENGINE=InnoDB;

INSERT INTO User (FullName, Username, Email, Password, IsAdmin, NightMode, LoggedIn) VALUES
    ("Bob Wilson", "scarface", "bob@bob.com", "def123B-2", 0, 0, 0),
    ("Geoff Smith", "goofy", "goofy@hotmail.com", "g00f8a11", 0, 1, 0),
    ("Emily Stevens", "lala", "lala@tinkywinky.com", "ttsay88", 0, 1, 0),
    ("Chris Davies", "shushu", "shushu@gmail.com", "jiggy_66", 0, 1, 0),
    ("Max Burleigh", "admin",  "admin@digz.com", "pass123", 1, 0, 0);

INSERT INTO ContactDetail (PostCode, PhoneNumber, Address) VALUES
    ("LS29 9AN", "07893 896011", "20 Kings Road, Ilkley, West Yorkshire"),
    ("LS29 9NE", "07893 895033", "19 Kings Road, Ilkley, West Yorkshire");

INSERT INTO PaymentDetail (CardNumber, ExpiryDate) VALUES
    ("4865 0194 7244 7883", "06/22");

INSERT INTO Bike (Model, Maintenance, LastMaintenance, InUse, UserID) VALUES
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Mens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),

    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL),
    ("Womens", 0, '2021-03-20 14:00:00', 0, NULL);

INSERT INTO Trip (TotalDist, AvgSpeed, CaloriesBurned, TotalHours, TotalSpend, UserID, BikeID) VALUES
    (33.43, 10.50, 50.60, 13.32, 13.50, 1, 3);

INSERT INTO Transaction (Payment, Balance, Date, UserID, TripID) VALUES
    (19.75, 101.45, '2021-03-26 14:30:00', 2, 1);

INSERT INTO Accident (Description, Injury, Damage, Date, UserID, BikeID) VALUES
    ("Fell off the bike", "Grazed knee", "Scratch on paintwork", "2021-03-27 15:30:00", 2, 3);
