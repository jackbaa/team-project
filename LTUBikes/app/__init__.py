from flask import Flask
from config import DockerDevConfig
from flaskext.mysql import MySQL

app = Flask(__name__)
app.secret_key = "secret key"
app.config.from_object(DockerDevConfig)

mysql = MySQL()

mysql.init_app(app)

from app import routes
