from flask import render_template, flash, redirect, url_for, request, session
#from flask_mysqldb import MySQL
from app import app, mysql
import bcrypt
#import MySQLdb.cursors
import re
global loggedIn
loggedIn = False

global data #this variable will store username data and the logout button, which will appear on login
data = ['', '', '']


@app.route('/', methods=['GET'])
@app.route('/index', methods=['GET'])
def index():
    return render_template('index.html', title='Home', data=data)

#Route for logging in
@app.route('/login', methods=['GET', 'POST'])
def login():
    global loggedIn
    if loggedIn == True:
        flash("You're already logged in!")
        return redirect(url_for('index'))
    if request.method == 'POST': #If the user has SENT data to the server
        username = request.form.get('username') #store the entered username
        password = request.form.get('password') #store the entered password
        cursor = mysql.get_db().cursor() #posistion in db
        cursor.execute('SELECT * FROM User WHERE username = % s AND password = % s', (username, password)) #execute this query
        account = cursor.fetchall() #store the record in this variable
        if account:
            session['username'] = account[0]
            data[0] = 'Logged in as: '
            data[1] = username
            data[2] = 'Logout'
            msg = 'Logged in successfully!'
            loggedIn = True
            return render_template('index.html', msg = msg, title = 'Home', data = data)
        else:
            msg = 'Incorrect username/password!'
            flash(msg)
    return render_template('login.html',  title='Sign In', data=data)

@app.route('/register', methods=['GET', 'POST'])
def register():
    global loggedIn
    msg = ''
    if loggedIn == True:
        flash("You're already logged in!")
        return redirect(url_for('index'))
    if request.method == 'POST':
        name = request.form['name']
        username = request.form['uname']
        psw = request.form['psw']
        email = request.form['email']
        isadmin = 0
        cursor = mysql.get_db().cursor()
        cursor.execute('SELECT * FROM User WHERE username = % s', (username))
        account = cursor.fetchall()
        if account:
             session['username'] = account[0]
             msg = 'Account already exists!'
             flash(msg)
        else:
            cursor.execute('INSERT INTO User (FullName, Username, Email, Password, IsAdmin) VALUES (%s, %s, %s, %s, %s)', (name, username, email, psw, isadmin))
            mysql.get_db().commit()
            loggedIn = True
            msg = 'You have successfully registered!'
            flash(msg)
            data[0] = 'logged in as: '
            data[1] = username
            data[2] = 'logout'
            return render_template('index.html', msg = msg, data=data)
    return render_template('register.html', title='Register', msg = msg, data=data)




@app.route('/profile', methods=['GET'])
def profile():
    if loggedIn == True:
        cursor = mysql.get_db().cursor()
        cursor.execute('SELECT FullName FROM User WHERE username = % s', (data[1]))
        fullName = cursor.fetchall()
        fullName = re.sub(r'\W+', '', str(fullName))
        data.append(fullName)
        return render_template('profile.html', title='My profile', data = data)
    else:
        flash('Please login first')
        return redirect(url_for('index'))



@app.route('/profile/export', methods=['GET'])
def export():
    return render_template('export.html', title='Export', data = data)



@app.route('/bikes', methods=['GET'])
def bikes():
    if loggedIn == True:
        return render_template('bikes.html', title='Bikes', data = data)
    else:
        flash('Please login first')
        return redirect(url_for('index'))



@app.route('/about', methods=['GET'])
def about():
    return render_template('about.html', title='About', data = data)



@app.route('/logout', methods=['GET'])
def logout():
    global data
    data = ['', '', '']
    account = None
    global loggedIn
    loggedIn = False
    msg = 'Successfully logged out'
    flash(msg)
    return render_template('index.html', msg = msg, data = data)

@app.route('/bikes/blue', methods=['GET'])
def bookBlue():
    if loggedIn == True:
        return render_template('bookblue.html', data=data, title='Blue')
    else:
        flash('Please login first')
        return redirect(url_for('index'))

@app.route('/bikes/pink', methods=['GET'])
def bookPink():
    if loggedIn == True:
        return render_template('bookpink.html', data=data, title = 'Pink')
    else:
        flash('Please login first')
        return redirect(url_for('index'))

@app.route('/profile/settings', methods=['GET'])
def settings():
    username = data[1]
    cursor = mysql.get_db().cursor() #posistion in db
    cursor.execute('SELECT * FROM User WHERE username = % s', (username)) #execute this query
    account = cursor.fetchall()
    if account:
        session["NightMode"] = account[0]
        background = account[0]
    if loggedIn == True:
        return render_template('settings.html', data=data, background=background, title='Settings')
    else:
        flash('Please login first')
        return redirect(url_for('index'))

@app.route('/bikes/pay', methods=['GET', 'POST'])
def pay():
    if loggedIn == True:
        return render_template('pay.html', data=data, title='Pay')
    else:
        flash('You must be logged in to view this page')
        return redirect(url_for('index'))

@app.route('/bikes/pay/success', methods=['GET'])
def success():
    if loggedIn == True:
        cursor = mysql.get_db().cursor()
        cursor.execute('SELECT FullName FROM User WHERE username = % s', (data[1]))
        fullName = cursor.fetchall()
        fullName = re.sub(r'\W+', '', str(fullName))
        data.append(fullName)
        return render_template('success.html', data=data, title = 'Authorised')
    else:
        flash('You must be logged in to view this page')
        return redirect(url_for('index'))
