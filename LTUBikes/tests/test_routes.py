from flask import Flask

from team-project.LTUBikes.app.routes import *

def test_index_route():
    app = Flask(__name__)
    configure_routes(app)
    client = app.test.client()
    url = '/'

    response = client.get(url)
    assert response.get_data() == b"index.html"