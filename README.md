# Team Project
# This is our main repo for the Team project this has been used for keeping our code up-to-data and also for sharing code to each other
# The link for the webpage is https://bit.ly/3v79yx0

# To run the LTUBikes app on the local host you need to;  
* Open Powershell
* Cd into the team-project and then the LTUBikes  directory
* Then run: docker-compose up --build, to build the app
* Open your web browser and enter localhost:5000 in the browsers search bar.

# To run the LTUBikes app on the public server you need to;
* Go to the link https://portal.azure.com/#@df4c20ba-64a8-4352-b3f9-47881abbc09a/resource/subscriptions/bc8823e3-484e-4c6c-b6d3-85e79c503f6f/resourceGroups/4044-team-4_group/providers/Microsoft.Compute/virtualMachines/4044-team-4/overview and press start to start up the VM
* Download PuTTY using the link https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
* Open PuTTY
* Use the ip address 51.141.71.225
* Use the username team-4
* Once the linux VM is running use the command cd team-project/team-project/LTUBikes
* Once in to the directory use command sudo docker-compose up
* Open your browser on the link provided at the top of the page
